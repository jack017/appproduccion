module OrdenesHelper
    #mostrar el nombre de la talla con el  for del show ordenes
    def mostrarTalla(talla_id)
       talla = Talla.find_by id: talla_id
       return talla.nombre
    end
    
    #mostrar el nombre del color con el  for del show ordenes
    def mostrarColor(color_id)
       color = Color.find_by id: color_id
       return color.nombre
    end
end
