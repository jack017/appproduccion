class Orden < ActiveRecord::Base
  has_many :tallas
  belongs_to :tela
  belongs_to :referencia
  has_many :agregar_tallas
  
  #aceptar atributos cocoon
  accepts_nested_attributes_for :agregar_tallas, reject_if: :all_blank, allow_destroy: true
end
