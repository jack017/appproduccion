json.array!(@ordenes) do |orden|
  json.extract! orden, :id, :Fecha, :tela_id, :referencia_id, :cantidad, :descripcion
  json.url orden_url(orden, format: :json)
end
