class CreateTelas < ActiveRecord::Migration
  def change
    create_table :telas do |t|
      t.string :Referencia
      t.string :nombre
      t.text :descripcion

      t.timestamps null: false
    end
  end
end
