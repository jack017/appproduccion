class CreateOrdenes < ActiveRecord::Migration
  def change
    create_table :ordenes do |t|
      t.date :Fecha
      t.references :tela, index: true, foreign_key: true
      t.references :referencia, index: true, foreign_key: true
      t.float :cantidad
      t.text :descripcion

      t.timestamps null: false
    end
  end
end
