class CreateAgregarTallas < ActiveRecord::Migration
  def change
    create_table :agregar_tallas do |t|
      t.references :color, index: true, foreign_key: true
      t.references :talla, index: true, foreign_key: true
      t.integer :cantidad
      t.belongs_to :orden, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
