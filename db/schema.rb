# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150916150914) do

  create_table "agregar_tallas", force: :cascade do |t|
    t.integer  "color_id"
    t.integer  "talla_id"
    t.integer  "cantidad"
    t.integer  "orden_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "agregar_tallas", ["color_id"], name: "index_agregar_tallas_on_color_id"
  add_index "agregar_tallas", ["orden_id"], name: "index_agregar_tallas_on_orden_id"
  add_index "agregar_tallas", ["talla_id"], name: "index_agregar_tallas_on_talla_id"

  create_table "colors", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documentos", force: :cascade do |t|
    t.string   "siglas"
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ordenes", force: :cascade do |t|
    t.date     "Fecha"
    t.integer  "tela_id"
    t.integer  "referencia_id"
    t.float    "cantidad"
    t.text     "descripcion"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "ordenes", ["referencia_id"], name: "index_ordenes_on_referencia_id"
  add_index "ordenes", ["tela_id"], name: "index_ordenes_on_tela_id"

  create_table "tallas", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "telas", force: :cascade do |t|
    t.string   "Referencia"
    t.string   "nombre"
    t.text     "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "zoomers", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

end
